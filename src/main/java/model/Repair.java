package model;

import exceptions.InformationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.LazyInitializationException;

import javax.persistence.*;

/**
 * @author Daniil Kudiyarov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "repair")
public class Repair {

    @Id
    @SequenceGenerator(name = "autoIncrementRepair", sequenceName = "AUTO_INCREMENT_REPAIR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoIncrementRepair")
    @Column(unique = true)
    private int id;

    @Column(name = "description", nullable = false)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "center_id", nullable = false)
    private CarServiceCenter center;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

    public Repair(String description, int centerID, int employeeID) {
        this.description = description;
        this.center = new CarServiceCenter(centerID);
        this.employee = new Employee(employeeID);
    }

    public Repair(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Repair[" +
                "ID:" + id +
                ", Description:" + description + ']';
    }

    public String toStringFullInformation() {
        try {
            return "Repair[" + "ID:" + id + ", Description:" + description + "]" +
                    "\n" +
                    this.employee.toString() +
                    "\n" +
                    this.center.toString();
        } catch (LazyInitializationException ex) {
            throw new InformationException("Full information is not available");
        }
    }
}

package interfaces;

import com.sun.istack.NotNull;
import lombok.NonNull;

import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
public interface DAO<Entity, Key> {

    void create(@NonNull final Entity entity);

    Entity read(@NonNull final Key id);

    void update(@NonNull final Entity entity);

    void delete(@NonNull final Entity entity);

    Entity readFullInformation(@NonNull final Key id);

    List<Entity> readAll();
}

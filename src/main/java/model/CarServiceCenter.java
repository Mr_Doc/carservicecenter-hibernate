package model;

import exceptions.InformationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.LazyInitializationException;

import javax.persistence.*;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "car_service_center")
public class CarServiceCenter {

    @Id
    @SequenceGenerator(name = "autoIncrementCarServiceCenter", sequenceName = "AUTO_INCREMENT_CAR_SERVICE_CENTER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoIncrementCarServiceCenter")
    @Column(unique = true)
    private int id;

    @Column(name = "title", unique = true, nullable = false)
    private String title;

    @Column(name = "phone", unique = true, nullable = false)
    private String phone;

    @Column(name = "address", unique = true, nullable = false)
    private String address;

    @OneToMany(mappedBy = "center", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Employee> employees;

    @OneToMany(mappedBy = "center", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Repair> repairs;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", nullable = false)
    private City city;

    @ManyToMany
    @JoinTable(name = "service_customer",
            joinColumns = @JoinColumn(name = "service_id"),
            inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private List<Customer> customers;

    public CarServiceCenter(String title, String phone, String address, int cityID) {
        this.title = title;
        this.phone = phone;
        this.address = address;
        this.city = new City(cityID);
    }

    public CarServiceCenter(int centerID) {
        this.id = centerID;
    }


    @Override
    public String toString() {
        return "CarServiceCenter[" +
                "ID:" + this.id +
                ",Title:" + this.title +
                ",Phone:" + this.phone +
                ",Address:" + this.address + "]";
    }


    public String toStringFullInformation() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("CarServiceCenter[")
                    .append("ID:").append(this.id)
                    .append(",Title:").append(this.title)
                    .append(",Phone:").append(this.phone)
                    .append(",Address:").append(this.address);
            sb.append("\n");
            sb.append(this.city.toString());
            sb.append("\n");
            sb.append("Center Employees:\n");
            for (Employee i : employees) {
                sb.append(i.toString()).append("\n");
            }
            sb.append("Center Repairs:\n");
            for (Repair i : repairs) {
                sb.append(i.toString()).append("\n");
            }
            return sb.toString();
        } catch (LazyInitializationException ex) {
            throw new InformationException("Full information is not available");
        }
    }
}

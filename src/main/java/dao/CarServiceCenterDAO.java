package dao;

import interfaces.DAO;
import lombok.NonNull;
import model.CarServiceCenter;
import model.City;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
public class CarServiceCenterDAO implements DAO<CarServiceCenter, Integer> {

    private final SessionFactory factory;

    public CarServiceCenterDAO(@NonNull final SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(@NonNull CarServiceCenter carServiceCenter) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(carServiceCenter);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public CarServiceCenter read(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(CarServiceCenter.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void update(@NonNull CarServiceCenter carServiceCenter) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(carServiceCenter);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(@NonNull CarServiceCenter carServiceCenter) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(carServiceCenter);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public CarServiceCenter readFullInformation(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            final CarServiceCenter result = session.get(CarServiceCenter.class, id);
            if (result != null) {
                Hibernate.initialize(result.getCity());
                Hibernate.initialize(result.getEmployees());
                Hibernate.initialize(result.getCustomers());
                Hibernate.initialize(result.getRepairs());
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    @Override
    public List<CarServiceCenter> readAll() {
        try (final Session session = factory.openSession()) {
            return session.createQuery("FROM CarServiceCenter ", CarServiceCenter.class).getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


}

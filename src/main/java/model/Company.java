package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Daniil Kudiyarov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "customer_company")
public class Company extends Customer {

    @Id
    @SequenceGenerator(name = "autoIncrementCompany", sequenceName = "AUTO_INCREMENT_CUSTOMER_COMPANY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoIncrementCompany")
    @Column(unique = true)
    private int id;

    @Column(name = "inn")
    private String inn;

    public Company(String email, String phone, CarServiceCenter center, String inn) {
        super(email, phone, center);
        this.inn = inn;
    }

    @Override
    public String toString() {
        return "Company[" +
                "ID:" + id + "," +
                super.toString() +
                "INN:" + inn + "]\n";
    }
}

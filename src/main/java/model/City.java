package model;

import exceptions.InformationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.LazyInitializationException;

import javax.persistence.*;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "city")
public class City {

    @Id
    @SequenceGenerator(name = "autoIncrementCity", sequenceName = "AUTO_INCREMENT_CITY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoIncrementCity")
    @Column(unique = true)
    private int id;

    @Column(name = "title",
            unique = true,
            nullable = false)
    private String title;

    @Column(name = "code",
            unique = true,
            nullable = false)
    private String code;

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CarServiceCenter> centers;

    public City(String title, String code) {
        this.title = title;
        this.code = code;
    }

    public City(int cityID) {
        this.id = cityID;
    }

    @Override
    public String toString() {
        return "City[" +
                "ID:" + id + "," +
                "Title:" + title + "," +
                "Code:" + code + "]";
    }


    public String toStringFullInformation() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(this.toString());
            sb.append("\n");
            for (CarServiceCenter serviceCenter : centers) {
                sb.append(serviceCenter.toString()).append("\n");
            }
            return sb.toString();
        } catch (LazyInitializationException ex) {
            throw new InformationException("Full information is not available");
        }

    }
}

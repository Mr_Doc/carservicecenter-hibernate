package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @SequenceGenerator(name = "autoIncrementEmployee", sequenceName = "AUTO_INCREMENT_Employee", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoIncrementEmployee")
    @Column(unique = true)
    private int id;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "birth_date", nullable = false)
    private Date birthDate;

    @Column(name = "email")
    private String email;

    @Column(name = "salary", nullable = false)
    private BigDecimal salary;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Repair> repairs;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "center_id", nullable = false)
    private CarServiceCenter center;

    public Employee(String lastName, String firstName, Date date, String email, BigDecimal salary, int centerID) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = date;
        this.email = email;
        this.salary = salary;
        this.center = new CarServiceCenter(centerID);
    }

    public Employee(int employeeID) {
        this.id = employeeID;
    }


    @Override
    public String toString() {
        return "Employee[" +
                "ID:" + id +
                ", LastName:" + lastName +
                ", FirstName:" + firstName +
                ", BirthDate:" + birthDate +
                ", Email:" + email +
                ", Salary:" + salary +
                ']';
    }

    public String toStringFullInformation() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("Employee[" +
                    "ID:").append(id)
                    .append(", LastName:").append(lastName)
                    .append(", FirstName:").append(firstName)
                    .append(", BirthDate:").append(birthDate)
                    .append(", Email:").append(email)
                    .append(", Salary:").append(salary)
                    .append(",").append(this.center.toString()).append("]");
            sb.append("\n");
            sb.append("Employee Repairs:");
            for (Repair i : this.repairs) {
                sb.append(i.toString()).append("\n");
            }
            return sb.toString();
        } catch (Exception ex) {
            return "Full information is not available";
        }
    }
}

package model;

import exceptions.InformationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.LazyInitializationException;

import javax.persistence.*;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @SequenceGenerator(name = "autoIncrementCustomer", sequenceName = "AUTO_INCREMENT_CUSTOMER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoIncrementCustomer")
    @Column(unique = true)
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(name = "service_customer",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private List<CarServiceCenter> centers;


    public Customer(String email, String phone, CarServiceCenter center) {
        this.email = email;
        this.phone = phone;
        if (this.centers == null) {
            this.centers = new ArrayList<>();
        }
        this.centers.add(center);
    }

    @Override
    public String toString() {
        return "Name:" + this.name + "," +
                "Email:" + this.email + "," +
                "Phone:" + this.phone + ",";
    }

    public String toStringFullInformation() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(this.toString());
            for (CarServiceCenter i : this.centers) {
                sb.append(i.toString());
                sb.append("\n");
            }
            return sb.toString();
        } catch (LazyInitializationException ex) {
            throw new InformationException("Full information is not available");
        }
    }
}

package dao;

import interfaces.DAO;
import lombok.NonNull;
import model.City;
import model.Employee;
import model.Repair;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
public class RepairDAO implements DAO<Repair, Integer> {

    private final SessionFactory factory;

    public RepairDAO(@NonNull final SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(@NonNull Repair repair) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(repair);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Repair read(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(Repair.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void update(@NonNull Repair repair) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(repair);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(@NonNull Repair repair) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(repair);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Repair readFullInformation(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            Repair result = session.get(Repair.class, id);
            if (result != null) {
                Hibernate.initialize(result.getCenter());
                Hibernate.initialize(result.getEmployee());
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    @Override
    public List<Repair> readAll() {
        try (final Session session = factory.openSession()) {
            return session.createQuery("FROM Repair ", Repair.class).getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Daniil Kudiyarov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "customer_individual")
public class Individual extends Customer {

    @Id
    @SequenceGenerator(name = "autoIncrementIndividual", sequenceName = "AUTO_INCREMENT_INDIVIDUAL", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autoIncrementIndividual")
    @Column(unique = true)
    private int id;

    @Column(name = "passportNO", unique = true)
    private String passportNO;

    public Individual(String email, String phone, CarServiceCenter center, String passportNO) {
        super(email, phone, center);
        this.passportNO = passportNO;
    }


    @Override
    public String toString() {
        return "ID:" + id + "," +
                super.toString() +
                "PassportNO:" + passportNO + "]\n";
    }
}

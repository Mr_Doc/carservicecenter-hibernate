package exceptions;

import org.hibernate.LazyInitializationException;

/**
 * @author Daniil Kudiyarov
 */
public class InformationException extends LazyInitializationException {


    /**
     * Constructs a LazyInitializationException using the given message.
     *
     * @param message A message explaining the exception condition
     */
    public InformationException(String message) {
        super(message);
    }
}

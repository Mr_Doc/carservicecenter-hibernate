import dao.*;
import interfaces.DAO;
import model.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * @author Daniil Kudiyarovemployees
 */
public class Test {
    public static void main(String[] args) {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            DAO<City, Integer> dao = new CityDAO(factory);
            City city = new City("test", "test");
            dao.create(city);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}


package dao;

import interfaces.DAO;
import lombok.NonNull;
import model.CarServiceCenter;
import model.City;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
public class CityDAO implements DAO<City, Integer> {

    private final SessionFactory factory;

    public CityDAO(@NonNull final SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(@NonNull City city) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(city);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public City read(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(City.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void update(@NonNull City city) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(city);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(@NonNull City city) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(city);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public City readFullInformation(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            final City result = session.get(City.class, id);
            if (result != null) {
                Hibernate.initialize(result.getCenters());
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public List<City> readAll() {
        try (final Session session = factory.openSession()) {
            return session.createQuery("FROM City", City.class).getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


}

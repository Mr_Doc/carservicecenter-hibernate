package dao;

import interfaces.DAO;
import lombok.NonNull;
import model.City;
import model.Customer;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
public class CustomerDAO implements DAO<Customer, Integer> {

    private final SessionFactory factory;

    public CustomerDAO(@NonNull final SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(@NonNull Customer customer) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(customer);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Customer read(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(Customer.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void update(@NonNull Customer customer) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(customer);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(@NonNull Customer customer) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(customer);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Customer readFullInformation(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            Customer result = session.get(Customer.class, id);
            if (result != null) {
                Hibernate.initialize(result.getCenters());
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Customer> readAll() {
        try (final Session session = factory.openSession()) {
            return session.createQuery("FROM Customer ", Customer.class).getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}

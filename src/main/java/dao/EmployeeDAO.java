package dao;

import interfaces.DAO;
import lombok.NonNull;
import model.City;
import model.Employee;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;

/**
 * @author Daniil Kudiyarov
 */
public class EmployeeDAO implements DAO<Employee, Integer> {


    private final SessionFactory factory;

    public EmployeeDAO(@NonNull final SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void create(@NonNull Employee employee) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Employee read(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            return session.get(Employee.class, id);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void update(@NonNull Employee employee) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(employee);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(@NonNull Employee employee) {
        try (final Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(employee);
            session.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Employee readFullInformation(@NonNull Integer id) {
        try (final Session session = factory.openSession()) {
            Employee result = session.get(Employee.class, id);
            if (result != null) {
                Hibernate.initialize(result.getRepairs());
                Hibernate.initialize(result.getCenter());
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    @Override
    public List<Employee> readAll() {
        try (final Session session = factory.openSession()) {
            return session.createQuery("FROM Employee ", Employee.class).getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
